/* ВЫГРУЗКА MYSQL `internet_shop` - база данных личного кабинета заказчика


-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Окт 19 2015 г., 19:01
-- Версия сервера: 5.6.17
-- Версия PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES cp1251 */;

/*
-- База данных: `internet_shop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id_client` int(11) NOT NULL AUTO_INCREMENT,
  `guid_client` char(36) CHARACTER SET cp1251 COLLATE cp1251_bin NOT NULL,
  `client_fullname` varchar(255) COLLATE cp1251_ukrainian_ci NOT NULL,
  `phone_number` varchar(255) COLLATE cp1251_ukrainian_ci NOT NULL,
  PRIMARY KEY (`id_client`),
  UNIQUE KEY `guid_client` (`guid_client`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`id_client`, `guid_client`, `client_fullname`, `phone_number`) VALUES
(5, '6b891621-71a0-11e5-ab38-74d02b08d526', 'Сидоров А.К., ЧП', '487-89-87'),
(6, 'bfb1d3fe-71a0-11e5-ab38-74d02b08d526', 'Петров П.У., ЧП', '875-52-14');

--
-- Триггеры `clients`
--
DROP TRIGGER IF EXISTS `gen_guid_cl`;
DELIMITER //
CREATE TRIGGER `gen_guid_cl` BEFORE INSERT ON `clients`
 FOR EACH ROW SET new.guid_client := (SELECT UUID())
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `guid_order` char(36) CHARACTER SET cp1251 COLLATE cp1251_bin NOT NULL,
  `client_id` int(11) NOT NULL,
  `creation_date` datetime NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`id_order`),
  UNIQUE KEY `guid_order` (`guid_order`),
  KEY `client_id` (`client_id`),
  KEY `status_id` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id_order`, `guid_order`, `client_id`, `creation_date`, `status_id`) VALUES
(1, 'a9d29ef5-71a2-11e5-ab38-74d02b08d526', 5, '2015-10-12 00:00:03', 1),
(2, 'a9d2b127-71a2-11e5-ab38-74d02b08d526', 6, '2015-10-11 00:00:00', 1);

--
-- Триггеры `orders`
--
DROP TRIGGER IF EXISTS `gen_guid_ord`;
DELIMITER //
CREATE TRIGGER `gen_guid_ord` BEFORE INSERT ON `orders`
 FOR EACH ROW SET new.guid_order := (SELECT UUID())
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `orders_cl`
--
CREATE TABLE IF NOT EXISTS `orders_cl` (
`id_client` int(11)
,`guid_client` char(36)
,`client_fullname` varchar(255)
,`phone_number` varchar(255)
,`id_order` int(11)
,`guid_order` char(36)
,`client_id` int(11)
,`creation_date` datetime
,`status_id` int(11)
,`product_name` varchar(255)
);
-- --------------------------------------------------------

--
-- Структура таблицы `orders_products`
--

CREATE TABLE IF NOT EXISTS `orders_products` (
  `id_orders_products` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id_orders_products`),
  KEY `order_id` (`order_id`,`product_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `orders_products`
--

INSERT INTO `orders_products` (`id_orders_products`, `order_id`, `product_id`) VALUES
(1, 1, 3),
(2, 1, 4),
(4, 2, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id_product` int(11) NOT NULL AUTO_INCREMENT,
  `guid_product` char(36) CHARACTER SET cp1251 COLLATE cp1251_bin NOT NULL,
  `product_name` varchar(255) COLLATE cp1251_ukrainian_ci NOT NULL,
  PRIMARY KEY (`id_product`),
  UNIQUE KEY `guid_product` (`guid_product`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id_product`, `guid_product`, `product_name`) VALUES
(3, '4b1572da-71a2-11e5-ab38-74d02b08d526', 'Asus EeeBook X205TA (X205TA-FD015B) Dark Blue'),
(4, '4b157a3f-71a2-11e5-ab38-74d02b08d526', 'Acer Aspire E3-112-C16G (NX.MRNEU.005) Blue');

--
-- Триггеры `products`
--
DROP TRIGGER IF EXISTS `gen_guid_prod`;
DELIMITER //
CREATE TRIGGER `gen_guid_prod` BEFORE INSERT ON `products`
 FOR EACH ROW SET new.guid_product := (SELECT UUID())
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `statuses`
--

CREATE TABLE IF NOT EXISTS `statuses` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `status_description` varchar(255) COLLATE cp1251_ukrainian_ci NOT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `statuses`
--

INSERT INTO `statuses` (`id_status`, `status_description`) VALUES
(1, 'новий'),
(2, 'оброблений');

-- --------------------------------------------------------

--
-- Структура для представления `orders_cl`
--
DROP TABLE IF EXISTS `orders_cl`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `orders_cl` AS select `clients`.`id_client` AS `id_client`,`clients`.`guid_client` AS `guid_client`,`clients`.`client_fullname` AS `client_fullname`,`clients`.`phone_number` AS `phone_number`,`orders`.`id_order` AS `id_order`,`orders`.`guid_order` AS `guid_order`,`orders`.`client_id` AS `client_id`,`orders`.`creation_date` AS `creation_date`,`orders`.`status_id` AS `status_id`,`products`.`product_name` AS `product_name` from (((`orders` left join `clients` on((`orders`.`client_id` = `clients`.`id_client`))) left join `orders_products` on((`orders`.`id_order` = `orders_products`.`order_id`))) left join `products` on((`products`.`id_product` = `orders_products`.`product_id`)));

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id_client`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id_status`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `orders_products`
--
ALTER TABLE `orders_products`
  ADD CONSTRAINT `orders_products_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id_product`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `orders_products_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id_order`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/* ВЫГРУЗКА MYSQL `manager_base` - база данных менеджера

/*-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Окт 19 2015 г., 19:07
-- Версия сервера: 5.6.17
-- Версия PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES cp1251 */;

/*--
-- База данных: `manager_base`
--

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id_client` int(11) NOT NULL AUTO_INCREMENT,
  `guid_client` char(36) CHARACTER SET cp1251 COLLATE cp1251_bin NOT NULL,
  `client_fullname` varchar(255) COLLATE cp1251_ukrainian_ci NOT NULL,
  `phone_number` varchar(255) COLLATE cp1251_ukrainian_ci NOT NULL,
  `max_personal_discount` float NOT NULL,
  PRIMARY KEY (`id_client`),
  UNIQUE KEY `guid_client` (`guid_client`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`id_client`, `guid_client`, `client_fullname`, `phone_number`, `max_personal_discount`) VALUES
(5, '6b891621-71a0-11e5-ab38-74d02b08d526', 'Сидоров А.К., ЧП', '487-89-87', 12),
(6, 'bfb1d3fe-71a0-11e5-ab38-74d02b08d526', 'Петров П.У., ЧП', '875-52-14', 15.5);

--
-- Триггеры `clients`
--
DROP TRIGGER IF EXISTS `gen_guid_cl`;
DELIMITER //
CREATE TRIGGER `gen_guid_cl` BEFORE INSERT ON `clients`
 FOR EACH ROW SET new.guid_client := (SELECT UUID())
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `managers`
--

CREATE TABLE IF NOT EXISTS `managers` (
  `id_manager` int(11) NOT NULL AUTO_INCREMENT,
  `manager_name` varchar(255) COLLATE cp1251_ukrainian_ci NOT NULL,
  PRIMARY KEY (`id_manager`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `managers`
--

INSERT INTO `managers` (`id_manager`, `manager_name`) VALUES
(1, 'Пшенична'),
(2, 'Коваленко');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `guid_order` char(36) CHARACTER SET cp1251 COLLATE cp1251_bin NOT NULL,
  `client_id` int(11) NOT NULL,
  `creation_date` datetime NOT NULL,
  `status_id` int(11) NOT NULL,
  `manager_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_order`),
  UNIQUE KEY `guid_order` (`guid_order`),
  KEY `client_id` (`client_id`),
  KEY `status_id` (`status_id`),
  KEY `manager_id` (`manager_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci AUTO_INCREMENT=34 ;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id_order`, `guid_order`, `client_id`, `creation_date`, `status_id`, `manager_id`) VALUES
(5, '8f2d8625-7304-11e5-ab38-74d02b08d526', 5, '2015-10-06 00:00:00', 1, NULL),
(32, 'a9d2b127-71a2-11e5-ab38-74d02b08d526', 6, '2015-10-11 00:00:00', 1, NULL),
(33, 'a9d29ef5-71a2-11e5-ab38-74d02b08d526', 5, '2015-10-12 00:00:00', 1, NULL);

--
-- Триггеры `orders`
--
DROP TRIGGER IF EXISTS `gen_guid_ord`;
DELIMITER //
CREATE TRIGGER `gen_guid_ord` BEFORE INSERT ON `orders`
 FOR EACH ROW IF (@DISABLE_TRIGGERS IS NULL) THEN
SET new.guid_order := (SELECT UUID());
END IF
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `orders_cl`
--
CREATE TABLE IF NOT EXISTS `orders_cl` (
`id_client` int(11)
,`guid_client` char(36)
,`client_fullname` varchar(255)
,`phone_number` varchar(255)
,`id_order` int(11)
,`guid_order` char(36)
,`client_id` int(11)
,`creation_date` datetime
,`status_id` int(11)
,`product_name` varchar(255)
);
-- --------------------------------------------------------

--
-- Структура таблицы `orders_products`
--

CREATE TABLE IF NOT EXISTS `orders_products` (
  `id_orders_products` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id_orders_products`),
  KEY `order_id` (`order_id`,`product_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci AUTO_INCREMENT=24 ;

--
-- Дамп данных таблицы `orders_products`
--

INSERT INTO `orders_products` (`id_orders_products`, `order_id`, `product_id`) VALUES
(21, 32, 4),
(22, 33, 3),
(23, 33, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id_product` int(11) NOT NULL AUTO_INCREMENT,
  `guid_product` char(36) CHARACTER SET cp1251 COLLATE cp1251_bin NOT NULL,
  `product_name` varchar(255) COLLATE cp1251_ukrainian_ci NOT NULL,
  PRIMARY KEY (`id_product`),
  UNIQUE KEY `guid_product` (`guid_product`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id_product`, `guid_product`, `product_name`) VALUES
(3, '4b1572da-71a2-11e5-ab38-74d02b08d526', 'Asus EeeBook X205TA (X205TA-FD015B) Dark Blue'),
(4, '4b157a3f-71a2-11e5-ab38-74d02b08d526', 'Acer Aspire E3-112-C16G (NX.MRNEU.005) Blue');

--
-- Триггеры `products`
--
DROP TRIGGER IF EXISTS `gen_guid_prod`;
DELIMITER //
CREATE TRIGGER `gen_guid_prod` BEFORE INSERT ON `products`
 FOR EACH ROW SET new.guid_product := (SELECT UUID())
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `statuses`
--

CREATE TABLE IF NOT EXISTS `statuses` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `status_description` varchar(255) COLLATE cp1251_ukrainian_ci NOT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `statuses`
--

INSERT INTO `statuses` (`id_status`, `status_description`) VALUES
(1, 'новий'),
(2, 'оброблений');

-- --------------------------------------------------------

--
-- Структура для представления `orders_cl`
--
DROP TABLE IF EXISTS `orders_cl`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `orders_cl` AS select `clients`.`id_client` AS `id_client`,`clients`.`guid_client` AS `guid_client`,`clients`.`client_fullname` AS `client_fullname`,`clients`.`phone_number` AS `phone_number`,`orders`.`id_order` AS `id_order`,`orders`.`guid_order` AS `guid_order`,`orders`.`client_id` AS `client_id`,`orders`.`creation_date` AS `creation_date`,`orders`.`status_id` AS `status_id`,`products`.`product_name` AS `product_name` from (((`orders` left join `clients` on((`orders`.`client_id` = `clients`.`id_client`))) left join `orders_products` on((`orders`.`id_order` = `orders_products`.`order_id`))) left join `products` on((`products`.`id_product` = `orders_products`.`product_id`)));

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id_client`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id_status`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`manager_id`) REFERENCES `managers` (`id_manager`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `orders_products`
--
ALTER TABLE `orders_products`
  ADD CONSTRAINT `orders_products_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id_product`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `orders_products_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id_order`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoqueryexchange;
import java.sql.*;

/**
 *
 * @author N
 */
public class Exchange {
    
    private Connection connToManagerDB;
    private Connection connToClientDB;
       
        private Connection connectToManagerDB_Start(){
            
            String url = "jdbc:mysql://localhost:3306/";
            String dbName = "manager_base?allowMultiQueries=true";
            String driver = "com.mysql.jdbc.Driver";
            String userName = "root"; 
            String password = "";
            try { 
                Class.forName(driver).newInstance();
                Connection conn = DriverManager.getConnection(url+dbName,userName,password);
                return conn;
	    } catch (Exception e) {
		e.printStackTrace();
                return null;
	    }

       }
       
        private void connectToManagerDB_Close() throws SQLException{

            connToManagerDB.close();

       } 
        
        private Connection connectToClientDB_Start(){
            
            String url = "jdbc:mysql://localhost:3306/";
            String dbName = "internet_shop?allowMultiQueries=true";
            String driver = "com.mysql.jdbc.Driver";
            String userName = "root"; 
            String password = "";
            try { 
                Class.forName(driver).newInstance();
                connToClientDB = DriverManager.getConnection(url+dbName,userName,password);
                return connToClientDB;
	    } catch (Exception e) {
		e.printStackTrace();
                return null;
	    }
       }
       
       private void connectToClientDB_Close() throws SQLException{

            connToClientDB.close();

       } 
       
       public String send_new_orders() throws SQLException{
                   
           connToManagerDB = connectToManagerDB_Start();
            if (connToManagerDB == null){
               return "Unable to connect to ManagerDB";
           }
            
           connToClientDB = connectToClientDB_Start();
            if (connToClientDB == null){
               connectToManagerDB_Close(); 
               return "Unable to connect to ClientDB";
           }
           
           Statement stmt_manager= connToManagerDB.createStatement();  
           Statement stmt_cl = connToClientDB.createStatement();
           
           //Считаем, что id_client и id_product совпадают в двух базах, так как клиенты и товары
           //создаются только в базе manager_base и после этого переносятся в БД internet_shop.
           //Заказы же могут создаваться как в базе manager_base так и internet_shop,
           //поэтому заказы сопоставляем по guid_order.
           //
           
           //выбираем из базы internet_shop все заказы со статусом НОВЫЙ
           ResultSet rs = stmt_cl.executeQuery( "SELECT \n" +
                    //    "	`internet_shop`.`orders`.`id_order` AS `id_order`,\n" +
                        "        `internet_shop`.`orders`.`guid_order` AS `guid_order`,\n" +
                        "	`internet_shop`.`orders`.`client_id` AS `id_client`,\n" +
                        "        `internet_shop`.`clients`.`guid_client` AS `guid_client`,\n" +
                        "        `internet_shop`.`orders`.`creation_date` AS `creation_date`,\n" +
                        "        `internet_shop`.`orders`.`status_id` AS `status_id`, \n" +
                        "        `internet_shop`.`products`.`id_product` as `id_product`,\n" +
                        "	`internet_shop`.`products`.`guid_product` as `guid_product`\n" +
                        "    FROM\n" +
                        "        (`internet_shop`.`orders`\n" +
                        "        LEFT JOIN `internet_shop`.`clients` ON ((`internet_shop`.`orders`.`client_id` = `internet_shop`.`clients`.`id_client`))\n" +
                        "        LEFT JOIN orders_products on (orders.id_order = orders_products.order_id)\n" +
                        "        LEFT JOIN products on (products.id_product = orders_products.product_id))\n"+
                        "     WHERE `internet_shop`.`orders`.`status_id` = 1");
           
           
           ResultSet rsID_order;
           Integer id_order = 0;
           String guid_order = "";
           
           while (rs.next()){
               
               //запись в таблицу заказов базы менеджера нового заказа
               if (!( rs.getString("guid_order").equals(guid_order))) {
                   
                   //проверим, может этот заказ уже есть в БД менеджера
                   //тогда переходим к следующей записи
                    ResultSet rs_check_order = stmt_manager.executeQuery("SELECT * FROM `orders` WHERE `guid_order`= '" + rs.getString("guid_order") + "'");
                    if (rs_check_order.next()){

                        continue;
                    }
                    
                    stmt_manager.execute("BEGIN; SET @DISABLE_TRIGGERS=1;"                    
                       + " INSERT INTO `manager_base`.`orders`(`guid_order`, `client_id`,`creation_date`,`status_id`)" 
                       + " VALUES('" + rs.getString("guid_order") + "', "+ rs.getInt("id_client")
                       + ", '" + rs.getDate("creation_date") + "', " + rs.getInt("status_id") + ");"
                       + " SET @DISABLE_TRIGGERS=NULL; COMMIT;");
                    
                    //получаем id нового заказа в базе менеджера 
                    rsID_order = stmt_manager.executeQuery("SELECT `manager_base`.`orders`.`id_order` from `manager_base`.`orders` where orders.guid_order = '" + rs.getString("guid_order") + "'");
                    rsID_order.next();
                    id_order =  rsID_order.getInt("id_order");
                    
                    guid_order = rs.getString("guid_order");
               }
               
               
                //запись в таблицу orders_products товаров заказа
               stmt_manager.execute("INSERT INTO `manager_base`.`orders_products` (`order_id`, `product_id`)"
                       + " VALUES (" + id_order + ", " + rs.getInt("id_product") + ")");
               
               
           }

           connectToManagerDB_Close();
           connectToClientDB_Close();
           return "The orders were sent successfully!";
           
       }

      public String send_new_status(String guid_order) throws SQLException{
                   
           connToManagerDB = connectToManagerDB_Start();
            if (connToManagerDB == null){
               return "Unable to connect to ManagerDB";
           }
            
           connToClientDB = connectToClientDB_Start();
            if (connToClientDB == null){
               connectToManagerDB_Close(); 
               return "Unable to connect to ClientDB";
           }
           
           Statement stmt_manager= connToManagerDB.createStatement();  
           Statement stmt_cl = connToClientDB.createStatement();
           
           //получаем статус заказа с указанным guid в базе менеджера  
           ResultSet rs = stmt_manager.executeQuery("SELECT `manager_base`.`orders`.`status_id` from `manager_base`.`orders` where orders.guid_order = '" + guid_order + "'");
           if (rs.next()){
               stmt_cl.execute("Update `internet_shop`.`orders` SET `internet_shop`.`orders`.`status_id` = " + rs.getInt("status_id") + 
                       " WHERE `internet_shop`.`orders`.`guid_order` = '" + guid_order + "'" );
           } else {
                    return "There is no order with guid " + guid_order + " in ClientDB";
           }
           
           connectToManagerDB_Close();
           connectToClientDB_Close();
           
           return "The status was sent successfully!";
      }   
           
}

