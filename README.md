# README #

Пример выполнения лабораторной работы №2.

**Описание процесса.** Существуют 2 информационные системы: Интернет-магазин и АРМ Менеджера между которыми организован автоматический обмен. 
Юр. лицо оформляет заказ в Интернет-магазине путем выбора необходимых товаров и помещения их в корзину. Далее оно заполняет свои контактные данные и оплачивает заказ. В базу АРМ Менеджера автоматически загружаются новые заказы, оформленные в Интернет-магазине. Менеджер отправляет товары заказчику и устанавливает статус «Отправлен» его заказу. Измененный статус заказа автоматически меняется и в Интернет-магазине. 

____________________________________________________________

**IDEF0-диаграмма бизнес-процесса выглядит следующим образом** 

![ProgModelling_example_lr№1.png](https://bitbucket.org/repo/EE7Ezq/images/1619572607-ProgModelling_example_lr%E2%84%961.png)
____________________________________________________________

**Реализация**
Обмен производится между двумя базами данных: базой данных интернет магазина (internet_shop) и АРМ Менеджера (manager_base). 

Файлы для импорта баз данных MySQL

[internet_shop](https://bitbucket.org/Nataly_Chkhan/discipline-software-modelling/src/ee24db442ab700dfd759f8545bd20bcd06be6609/src/internet_shop%20%20DB%20structure?at=master&fileviewer=file-view-default)
[manager_base](https://bitbucket.org/Nataly_Chkhan/discipline-software-modelling/src/ee24db442ab700dfd759f8545bd20bcd06be6609/src/manager_base%20DB%20structure?at=master&fileviewer=file-view-default)

Обмен реализован на Java. Подробности реализации в комментариях.
https://bitbucket.org/Nataly_Chkhan/discipline-software-modelling/src/ee24db442ab7/src/demoqueryexchange/

____________________________________________________________






